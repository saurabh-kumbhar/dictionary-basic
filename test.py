import unittest
import os

import dictctl as target

class TestSum(unittest.TestCase):
    def test_list_int(self):
        api_key = os.environ.get('DICT_API_KEY')
        word = "exercise"
        word_pronunciation, word_type, word_definition = target.get_dictionary(word, api_key)

        expected = "ˈek-sər-ˌsīz (noun): the act of bringing into play or realizing in action"
        result = f"{word_pronunciation} ({word_type}): {word_definition}"
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()