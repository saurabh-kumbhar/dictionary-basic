# Dictionary Basic

This is a command line tool that will allow the user to query the merriam-webster
dictionary and receive a formatted response with the word definition.
https://dictionaryapi.com/products/api-collegiate-dictionary

## Usage

Prerequisite: [python](https://www.python.org/downloads/). 


```
./dictctl <word>
```

Output:
```
<pronounciation> (<type>): <definition>
```

## Example

```
./dictctl exercise
ek-sər-ˌsīz (noun): the act of bringing into play or realizing in action
```

## Pipeline

### build_dependencies
Create `venv` and install dependencies from `requirements.txt` required for `dictctl.py`. Stores artifact to be used by next jobs

### test_run
Run `test.py` to test function in `dictctl.py` which gets word definition

### publish_docker
Builds image using `Dockerfile` with required dependencies and push to repository

## Scope for Improvement
1. Handle error response 
2. Handle incorrectly spelled word
 