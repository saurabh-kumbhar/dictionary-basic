#!/usr/bin/env python3

import argparse
import requests
import os


def parse_argument():
    parser = argparse.ArgumentParser(description='Provide a word to get pronunciation and definition in return.')
    parser.add_argument('word_arg', help='Provide a word to get pronunciation and definition in return.')
    args = parser.parse_args()
    return args.word_arg


def get_dictionary(word, api_key):
    url = f"https://www.dictionaryapi.com/api/v3/references/collegiate/json/{word}?key={api_key}"
    
    response = requests.get(url)

    if response.status_code == 200:
        json_data = response.json()
        word_pronunciation = json_data[0]['hwi']['prs'][0]['mw']
        word_type = json_data[0]['fl']
        word_definition = json_data[0]['shortdef'][0].split(' :')[0]
        return word_pronunciation, word_type, word_definition
    else:
        print(f"Failed to retrieve details for a word '{word}'")
        return None


if __name__ == "__main__":
    
    api_key = os.environ.get('DICT_API_KEY')
    word = parse_argument()
    word_pronunciation, word_type, word_definition = get_dictionary(word, api_key)

    print()
    print('`{}` ({}): {}'.format(word_pronunciation, word_type, word_definition))
    print()

